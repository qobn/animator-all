package com.linji.myapplicationview

class RowBeam {
    var name:String?=null
    var index:Int?=null
    var fixRow:Int?=null // 静止列 仅能有一个
    var orientation:Int?=null // 0 向左移动  1 向右移动
    var moveStatus:Int?=null // 0静止  1 移动中 2 移动到中间并停止
    var moveDistance:Int?=null // 移动的总距离
    var currentDistance:Int?=null // 当前移动的距离
    var isMiddle:Boolean=false // 默认是否移动到中间 false
    var isAir:Boolean=false // 是否在通风 false
    override fun toString(): String {
        return "RowBeam(name=$name, fixRow=$fixRow, orientation=$orientation, moveStatus=$moveStatus, moveDistance=$moveDistance, currentDistance=$currentDistance)"
    }
}