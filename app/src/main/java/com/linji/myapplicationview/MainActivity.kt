package com.linji.myapplicationview

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.icu.text.CaseMap.Fold
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi

class MainActivity : AppCompatActivity() {

    var moveTime:Long = 3000
    var moveBeans:ArrayList<RowBeam> = ArrayList()
    var moveView:ArrayList<View> = ArrayList()
    var moveAnimators:ArrayList<ObjectAnimator?> = ArrayList()
    var moveAnimatorSets:ArrayList<AnimatorSet?> = ArrayList()
    var one:ImageView?=null
    var two:ImageView?=null
    var thr:ImageView?=null
    var four:ImageView?=null
    var five:ImageView?=null
    var six:ImageView?=null
    var ll_parent:LinearLayout?=null
    var tv_stop:TextView?=null
    var tv_go:TextView?=null
    var tv_reset:TextView?=null
    var tv_left:TextView?=null
    var tv_right:TextView?=null
    var tv_air:TextView?=null
    var tv_set_one:TextView?=null
    var tv_set_middle:TextView?=null
    var tv_set_last:TextView?=null
    var tv_click:TextView?=null
    var selectionPosition = -1
    private var isMoveLeft = false
//    private var moveAnimator: ObjectAnimator? = null
    lateinit var dynamiclinearlayout:DynamicLinearLayout
    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        one = findViewById(R.id.one)
//        two = findViewById(R.id.two)
//        thr = findViewById(R.id.thr)
//        four = findViewById(R.id.four)
//        five = findViewById(R.id.five)
//        six = findViewById(R.id.six)
//        dynamiclinearlayout = findViewById(R.id.dynamiclinearlayout)
        tv_click = findViewById(R.id.tv_click)
        ll_parent = findViewById(R.id.ll_parent)
        tv_stop = findViewById(R.id.tv_stop)
        tv_go = findViewById(R.id.tv_go)
        tv_reset = findViewById(R.id.tv_reset)
        tv_air = findViewById(R.id.tv_air)
        tv_left = findViewById(R.id.tv_left)
        tv_right = findViewById(R.id.tv_right)

        tv_set_one = findViewById(R.id.tv_set_one)
        tv_set_middle = findViewById(R.id.tv_set_middle)
        tv_set_last = findViewById(R.id.tv_set_last)
        moveBeans.clear()
        var rowBeam = RowBeam()
        rowBeam.name="0"
        rowBeam.index=0
        rowBeam.fixRow=3
        rowBeam.orientation=0
        rowBeam.moveStatus=0
        rowBeam.moveDistance=80
        rowBeam.currentDistance=0
        rowBeam.isMiddle=false
        moveBeans.add(rowBeam)

        var rowBeam2 = RowBeam()
        rowBeam2.name="1"
        rowBeam2.index=1
        rowBeam2.fixRow=3
        rowBeam2.orientation=0
        rowBeam2.moveStatus=0
        rowBeam2.moveDistance=80
        rowBeam2.currentDistance=0
        rowBeam2.isMiddle=false
        moveBeans.add(rowBeam2)

        var rowBeam3 = RowBeam()
        rowBeam3.name="2"
        rowBeam3.index = 2
        rowBeam3.fixRow=3
        rowBeam3.orientation=0
        rowBeam3.moveStatus=0
        rowBeam3.moveDistance=80
        rowBeam3.currentDistance=0
        rowBeam3.isMiddle=false
        moveBeans.add(rowBeam3)

        var rowBeam4 = RowBeam()
        rowBeam4.name="3"
        rowBeam4.index=3
        rowBeam4.fixRow=3
        rowBeam4.orientation=0
        rowBeam4.moveStatus=0
        rowBeam4.moveDistance=80
        rowBeam4.currentDistance=0
        rowBeam4.isMiddle=false
        moveBeans.add(rowBeam4)

        var rowBeam5 = RowBeam()
        rowBeam5.name="4"
        rowBeam5.index=4
        rowBeam5.fixRow=3
        rowBeam5.orientation=1
        rowBeam5.moveStatus=0
        rowBeam5.moveDistance=80
        rowBeam5.currentDistance=0
        rowBeam5.isMiddle=true
        moveBeans.add(rowBeam5)

        var rowBeam6 = RowBeam()
        rowBeam6.name="5"
        rowBeam6.index=5
        rowBeam6.fixRow=3
        rowBeam6.orientation=1
        rowBeam6.moveStatus=0
        rowBeam6.moveDistance=80
        rowBeam6.currentDistance=0
        rowBeam6.isMiddle=false
        moveBeans.add(rowBeam6)

        moveView.clear()
        moveView.clear()
        for (index in 0..5){
            val linearLayout = LinearLayout(this) // Replace 'context' with your actual context
            val linearLayoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            linearLayoutParams.setMargins(30, 10, 30, 10) // Set left, top, right, bottom margins for the linearLayout
            linearLayout.layoutParams = linearLayoutParams
            linearLayout.orientation = LinearLayout.VERTICAL
            linearLayout.gravity = Gravity.CENTER
            linearLayout.minimumHeight = 600 // 设置最小高度

            val textView = TextView(this) // Replace 'context' with your actual context
            textView.text = " $index"
            val textViewParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            textViewParams.gravity = Gravity.CENTER
            textViewParams.topMargin = 50
            textViewParams.bottomMargin = 50
            textView.layoutParams = textViewParams

            val imageView = ImageView(this)
            imageView.setBackgroundResource(R.mipmap.ic_launcher)
            val imageViewParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            imageViewParams.topMargin = 50
            imageViewParams.bottomMargin = 50
            imageViewParams.gravity = Gravity.CENTER
            imageView.layoutParams = imageViewParams

            linearLayout.addView(textView)
            linearLayout.addView(imageView)
            moveView.add(linearLayout)
            ll_parent?.addView(linearLayout)
        }

        moveAnimators.add(null)
        moveAnimators.add(null)
        moveAnimators.add(null)
        moveAnimators.add(null)
        moveAnimators.add(null)
        moveAnimators.add(null)
        moveAnimatorSets.add(null)
        moveAnimatorSets.add(null)
        moveAnimatorSets.add(null)
        moveAnimatorSets.add(null)
        moveAnimatorSets.add(null)
        moveAnimatorSets.add(null)


        tv_set_one?.setOnClickListener {
            resetAnimation()
            moveBeans.clear()
            for (index in 0..5){
                if (index==0){
                    var rowBeam = RowBeam()
                    rowBeam.name="$index"
                    rowBeam.index=index
                    rowBeam.fixRow = 0
                    rowBeam.orientation = 1
                    rowBeam.moveStatus = 0
                    rowBeam.moveDistance = 80
                    rowBeam.currentDistance = 0
                    rowBeam.isMiddle = false
                    rowBeam.isAir = false
                    moveBeans.add(rowBeam)
                }else{
                    var rowBeam = RowBeam()
                    rowBeam.name="$index"
                    rowBeam.index=index
                    rowBeam.fixRow = 0
                    rowBeam.orientation = 1
                    rowBeam.moveStatus = 0
                    rowBeam.moveDistance = 80
                    if (index==4){
                        rowBeam.currentDistance = 40
                        rowBeam.isMiddle = false
                    }else{
                        rowBeam.currentDistance = 0
                        rowBeam.isMiddle = false
                    }

                    rowBeam.isAir = false
                    moveBeans.add(rowBeam)
                }
            }
        }
        tv_set_middle?.setOnClickListener {
            resetAnimation()
            moveBeans.clear()
            for (index in 0..5){
                if (index<3){
                    var rowBeam = RowBeam()
                    rowBeam.name="$index"
                    rowBeam.index=index
                    rowBeam.fixRow = 3
                    rowBeam.orientation = 0
                    rowBeam.moveStatus = 0
                    rowBeam.moveDistance = 80
                    rowBeam.currentDistance = 0
                    rowBeam.isMiddle = false
                    rowBeam.isAir = false
                    moveBeans.add(rowBeam)
                }else if (index==3){
                    var rowBeam = RowBeam()
                    rowBeam.name="$index"
                    rowBeam.index=index
                    rowBeam.fixRow = 3
                    rowBeam.orientation = 1
                    rowBeam.moveStatus = 0
                    rowBeam.moveDistance = 80
                    rowBeam.currentDistance = 0
                    rowBeam.isMiddle = false
                    rowBeam.isAir = false
                    moveBeans.add(rowBeam)
                }else if (index>3){
                    var rowBeam = RowBeam()
                    rowBeam.name="$index"
                    rowBeam.index=index
                    rowBeam.fixRow = 3
                    rowBeam.orientation = 1
                    rowBeam.moveStatus = 0
                    rowBeam.moveDistance = 80
                    rowBeam.currentDistance = 0
                    rowBeam.isMiddle = false
                    rowBeam.isAir = false
                    moveBeans.add(rowBeam)
                }
            }
        }
        tv_set_last?.setOnClickListener {
            resetAnimation()
            moveBeans.clear()
            for (index in 0..5){
                if (index==5){
                    var rowBeam = RowBeam()
                    rowBeam.name="$index"
                    rowBeam.index=index
                    rowBeam.fixRow = 5
                    rowBeam.orientation = 0
                    rowBeam.moveStatus = 0
                    rowBeam.moveDistance = 80
                    rowBeam.currentDistance = 0
                    rowBeam.isMiddle = false
                    rowBeam.isAir = false
                    moveBeans.add(rowBeam)
                }else{
                    var rowBeam = RowBeam()
                    rowBeam.name="$index"
                    rowBeam.index=index
                    rowBeam.fixRow = 5
                    rowBeam.orientation = 0
                    rowBeam.moveStatus = 0
                    rowBeam.moveDistance = 80
                    rowBeam.currentDistance = 0
                    if (index==2){
                        //TODO 测试 移动一半
                        rowBeam.isMiddle = false
                    }else{
                        rowBeam.isMiddle = false
                    }
                    rowBeam.isAir = false
                    moveBeans.add(rowBeam)
                }
            }
        }
        tv_air?.setOnClickListener {
            if (moveBeans[0].isAir){
                Toast.makeText(this,"正在通风,禁止操作",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            resetAnimation()
            for (index in 0..<moveBeans.size){
                moveBeans[index].isAir = true
            }
            startAnimation(-100)
        }
        tv_stop?.setOnClickListener {
            stopAnimation(true)
        }
        tv_go?.setOnClickListener {
            resumeAnimation(-1)
        }
        tv_reset?.setOnClickListener {
            for (index in 0..<moveBeans.size){
                moveBeans[index].isAir = false
                moveBeans[index].moveDistance = 80
                moveBeans[index].currentDistance = 0
            }
            selectionPosition = -1
            resetAnimation()
        }
        tv_left?.setOnClickListener {
            if (selectionPosition==-1){
                Toast.makeText(this,"请选择移动列",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            isMoveLeft = true
            allChooseItem(isMoveLeft,selectionPosition)
        }
        tv_right?.setOnClickListener {
            if (selectionPosition==-1){
                Toast.makeText(this,"请选择移动列",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            isMoveLeft = false
            allChooseItem(isMoveLeft,selectionPosition)
        }
        for (view in moveView){
            view.setOnClickListener {
//                chooseItem(it, rowBeam)
                selectionPosition = moveView.indexOf(it)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun chooseItem(it: View, rowBeam: RowBeam) {
        val index = moveView.indexOf(it)
        val currentBean = moveBeans[index]
        if (currentBean.isAir) {
            Toast.makeText(this, "正在通风", Toast.LENGTH_SHORT).show()
            return
        }
        if (currentBean.fixRow == index) {
            Toast.makeText(this, "固定列不可移动", Toast.LENGTH_SHORT).show()
            return
        }
        //如果点的是这个并且它是暂停的，那么恢复其执行
//        if (rowBeam.moveStatus == 2) {
//            var moveAni = moveAnimators[index]
//            moveAni?.let {
//                if (it.isPaused) {
//                    resumeAnimation(index)
//                }
//            }
//        }

        val fixRow = currentBean.fixRow
        if (index < fixRow!!) {
            var isMoveLeft = false
            for (i in 0..index) {
                var currentBeanInside = moveBeans[i]
                if (currentBeanInside.orientation == 1) {
                    isMoveLeft = true
                }
            }
            if (!isMoveLeft) { // 从点击位置开始全部向左移动
                for (i in 0..index) {
                    Handler().postDelayed({
                        startAnimation(i)
                    }, (i * 1000).toLong())
    //                            startAnimation(i)
                }

            } else { // 如果内部有向左移动 并停止 该向右移动了  那么
                // 点击位置判断是该向左 还是向右  ；
                // 如果向右 那么判断右侧有没有元素 有的话 一起移动 ；
                // 如果向左那么判断左侧是 块移动 还是 单体移动
                if (currentBean.orientation == 0) {
                    // 固定列在 点击位置 右侧  初始方向应该向左
                    // 如果orientation = 0说明在原位置
                    // 在原位置的话 判断左侧有没有 向左移动过的 即 orientation=1 如果有则从移动过的首位 开始 到点击位置 移动
                    var moveIndex = -1
                    for (currentIndex in index - 1 downTo 0) {
                        if (moveBeans[currentIndex].orientation != 0) {
                            moveIndex = currentIndex
                            break
                        }
                    }
                    for (i in moveIndex + 1..index) {
                        Handler().postDelayed({
                            startAnimation(i)
                        }, (i * 1000).toLong())
    //                                startAnimation(currentIndex)
                    }
                    // 如果点击位置是向左移动过的 那么应该向右移动 判断 其右侧是否有 移动过的 如果有

                } else if (currentBean.orientation == 1) {
                    var moveIndex = -1
                    for (currentIndex in currentBean.index!!..<currentBean.fixRow!!) {
                        if (moveBeans[currentIndex].orientation == 1) {
                            moveIndex = currentIndex
                        }
                    }
                    for (i in currentBean.index!!..moveIndex) {
                        Handler().postDelayed({
                            startAnimation(i)
                        }, ((moveIndex - i) * 1000).toLong())
    //                                startAnimation(currentIndex)
                    }
                }
            }
        } else {
            var isMoveRight = false
            for (i in currentBean.fixRow!! + 1..<moveBeans.size) {
                var currentBeanInside = moveBeans[i]
                if (currentBeanInside.orientation == 0) {
                    isMoveRight = true
                }
            }
            if (!isMoveRight) { // 从点击位置开始全部向右移动
                for (i in moveBeans.size - 1 downTo index) {
                    Handler().postDelayed({
                        startAnimation(i)
                    }, ((moveBeans.size - 1 - i) * 1000).toLong())
    //                            startAnimation(i)
                }
            } else { // 如果内部有向右移动 并停止 该向做移动了  那么
                // 点击位置判断是该向左 还是向右  ；
                // 如果向左 那么判断左侧有没有元素 有的话 一起移动 ；
                // 如果向右那么判断右侧是 块移动 还是 单体移动
                if (currentBean.orientation == 1) {
                    // 固定列在 点击位置 右侧  初始方向应该向左
                    // 如果orientation = 0说明在原位置
                    // 在原位置的话 判断左侧有没有 向左移动过的 即 orientation=1 如果有则从移动过的首位 开始 到点击位置 移动
                    var moveIndex = -1
                    for (currentIndex in index..<moveBeans.size) {
                        if (moveBeans[currentIndex].orientation != 1) {
                            moveIndex = currentIndex
                            break
                        }
                    }
                    for (i in moveIndex - 1 downTo index) {
                        Handler().postDelayed({
                            startAnimation(i)
                        }, ((moveIndex - 1 - i) * 1000).toLong())
    //                                startAnimation(currentIndex)
                    }
                    // 如果点击位置是向左移动过的 那么应该向右移动 判断 其右侧是否有 移动过的 如果有
                } else if (currentBean.orientation == 0) {
                    var moveIndex = -1
                    for (currentIndex in currentBean.index!! downTo currentBean.fixRow!! + 1) {
                        if (moveBeans[currentIndex].orientation == 0) {
                            moveIndex = currentIndex
                        }
                    }
                    for (i in moveIndex..currentBean.index!!) {
                        Handler().postDelayed({
                            startAnimation(i)
                        }, ((i - moveIndex) * 1000).toLong())
    //                                startAnimation(currentIndex)
                    }
                }
            }
        }
    }
    @RequiresApi(Build.VERSION_CODES.O)
    private fun allChooseItem(isLeft:Boolean, chooseIndex:Int) {
        val currentBean = moveBeans[chooseIndex]
        if (currentBean.isAir) {
            Toast.makeText(this, "正在通风", Toast.LENGTH_SHORT).show()
            return
        }
        if (currentBean.fixRow == chooseIndex) {
            Toast.makeText(this, "固定列不可移动", Toast.LENGTH_SHORT).show()
            return
        }
        if (isLeft){
            if (currentBean.orientation==1){
                Toast.makeText(this,"向左不可移动",Toast.LENGTH_SHORT).show()
                return
            }
        }else{
            if (currentBean.orientation==0){
                Toast.makeText(this,"向右不可移动",Toast.LENGTH_SHORT).show()
                return
            }
        }
        //如果点的是这个并且它是暂停的，那么恢复其执行
//        if (currentBean.moveStatus == 2) {
//            var moveAni = moveAnimators[chooseIndex]
//            moveAni?.let {
//                if (it.isPaused) {
//                    resumeAnimation(chooseIndex)
//                }
//            }
//        }

        val fixRow = currentBean.fixRow
        if (chooseIndex < fixRow!!) {
            var isMoveLeft = false
            for (i in 0..chooseIndex) {
                var currentBeanInside = moveBeans[i]
                if (currentBeanInside.orientation == 1) {
                    isMoveLeft = true
                }
            }
            if (!isMoveLeft) { // 从点击位置开始全部向左移动
                for (i in 0..chooseIndex) {
                    Handler().postDelayed({
                        startAnimation(i)
                    }, (i * 1000).toLong())
                    //                            startAnimation(i)
                }
            } else { // 如果内部有向左移动 并停止 该向右移动了  那么
                // 点击位置判断是该向左 还是向右  ；
                // 如果向右 那么判断右侧有没有元素 有的话 一起移动 ；
                // 如果向左那么判断左侧是 块移动 还是 单体移动
                if (currentBean.orientation == 0) {
                    // 固定列在 点击位置 右侧  初始方向应该向左
                    // 如果orientation = 0说明在原位置
                    // 在原位置的话 判断左侧有没有 向左移动过的 即 orientation=1 如果有则从移动过的首位 开始 到点击位置 移动
                    var moveIndex = -1
                    for (currentIndex in chooseIndex - 1 downTo 0) {
                        if (moveBeans[currentIndex].orientation != 0) {
                            moveIndex = currentIndex
                            break
                        }
                    }
                    for (i in moveIndex + 1..chooseIndex) {
                        Handler().postDelayed({
                            startAnimation(i)
                        }, ((i-(moveIndex+1))*1000).toLong())
                        //                                startAnimation(currentIndex)
                    }
                    // 如果点击位置是向左移动过的 那么应该向右移动 判断 其右侧是否有 移动过的 如果有

                } else if (currentBean.orientation == 1) {
                    var moveIndex = -1
                    for (currentIndex in currentBean.index!!..<currentBean.fixRow!!) {
                        if (moveBeans[currentIndex].orientation == 1) {
                            moveIndex = currentIndex
                        }
                    }
                    for (i in currentBean.index!!..moveIndex) {
                        Handler().postDelayed({
                            startAnimation(i)
                        }, ((moveIndex - i) * 1000).toLong())
                        //                                startAnimation(currentIndex)
                    }
                }
            }
        } else {
            var isMoveRight = false
            for (i in currentBean.fixRow!! + 1..<moveBeans.size) {
                var currentBeanInside = moveBeans[i]
                if (currentBeanInside.orientation == 0) {
                    isMoveRight = true
                }
            }
            if (!isMoveRight) { // 从点击位置开始全部向右移动
                for (i in moveBeans.size - 1 downTo chooseIndex) {
                    Handler().postDelayed({
                        startAnimation(i)
                    }, ((moveBeans.size - 1 - i) * 1000).toLong())
                    //                            startAnimation(i)
                }
            } else { // 如果内部有向右移动 并停止 该向做移动了  那么
                // 点击位置判断是该向左 还是向右  ；
                // 如果向左 那么判断左侧有没有元素 有的话 一起移动 ；
                // 如果向右那么判断右侧是 块移动 还是 单体移动
                if (currentBean.orientation == 1) {
                    // 固定列在 点击位置 右侧  初始方向应该向左
                    // 如果orientation = 0说明在原位置
                    // 在原位置的话 判断左侧有没有 向左移动过的 即 orientation=1 如果有则从移动过的首位 开始 到点击位置 移动
                    var moveIndex = -1
                    for (currentIndex in chooseIndex..<moveBeans.size) {
                        if (moveBeans[currentIndex].orientation != 1) {
                            moveIndex = currentIndex
                            break
                        }
                    }
                    for (i in moveIndex - 1 downTo chooseIndex) {
                        Handler().postDelayed({
                            startAnimation(i)
                        }, ((moveIndex - 1 - i) * 1000).toLong())
                        //                                startAnimation(currentIndex)
                    }
                    // 如果点击位置是向左移动过的 那么应该向右移动 判断 其右侧是否有 移动过的 如果有
                } else if (currentBean.orientation == 0) {
                    var moveIndex = -1
                    for (currentIndex in currentBean.index!! downTo currentBean.fixRow!! + 1) {
                        if (moveBeans[currentIndex].orientation == 0) {
                            moveIndex = currentIndex
                        }
                    }
                    for (i in moveIndex..currentBean.index!!) {
                        Handler().postDelayed({
                            startAnimation(i)
                        }, ((i - moveIndex) * 1000).toLong())
                        //                                startAnimation(currentIndex)
                    }
                }
            }
        }
    }
    //    private fun startAnimation(index:Int,isMiddle:Boolean) {
//        val rowBeam = moveBeans[index] // Assuming you have only one element in moveBeans
//        val imageView: ImageView = moveView[index] as ImageView // Use the appropriate index
//
//        if (rowBeam.moveStatus == 0) { // Start animation only if not already moving
//            rowBeam.moveStatus = 1 // Set moveStatus to 1 (moving)
//            val currentTranslationX = imageView.translationX
//            val targetTranslationX = if (rowBeam.orientation == 0) {
//                currentTranslationX - rowBeam.moveDistance!!.toFloat()
//            } else {
//                currentTranslationX + rowBeam.moveDistance!!.toFloat()
//            }
//            var moveAnimator = ObjectAnimator.ofFloat(
//                imageView,
//                "translationX",
//                currentTranslationX,
//                targetTranslationX
//            )
//            if (moveAnimators[index]==null){
//                moveAnimators[index] = moveAnimator
//            }else{
//                moveAnimators[index]?.isPaused
//                resumeAnimation(index)
//            }
//            moveAnimator?.duration = moveTime // Set the duration of the animation in milliseconds
//            moveAnimator?.addListener(object : Animator.AnimatorListener {
//                override fun onAnimationStart(animation: Animator) {
//                    // Case 1: Normal Animation - Start
//                    if (isMiddle) {
//                        // Delay for half of the animation duration before stopping the animation
//                        Handler().postDelayed({
//                            stopAnimation()
//                        }, moveTime / 2)
//                    }
//                }
//
//                override fun onAnimationEnd(animation: Animator) {
//                    // Case 1: Normal Animation - End
//                    rowBeam.moveStatus = 0 // Set it to 2 for example, adjust as needed
//                    if (rowBeam.orientation == 0) {
//                        // If the current orientation is 0, set it to 1 (向右移动)
//                        rowBeam.orientation = 1
//                    } else {
//                        // If the current orientation is 1 or null, set it to 0 (向左移动)
//                        rowBeam.orientation = 0
//                    }
//                }
//
//                override fun onAnimationCancel(animation: Animator) {
//                    // Case 1: Normal Animation - Cancelled (if needed)
//                }
//
//                override fun onAnimationRepeat(animation: Animator) {
//                    // Case 1: Normal Animation - Repeated (if needed)
//                }
//            })
//
//            moveAnimator?.start()
//        } else if (rowBeam.moveStatus == 1) {
//            // Case 2: User Interaction - Animation is in progress, stop it
//            stopAnimation()
//        } else if (rowBeam.moveStatus == 2) {
//            // Case 3: User Interaction - Animation was paused, resume it
//            resumeAnimation(index)
//        }
//    }
    @RequiresApi(Build.VERSION_CODES.O)
    private fun startAnimation(index: Int) {
        if (index == -100) {
            for (i in moveView.indices) {
                val currentBean = moveBeans[i]
                val imageView: View = moveView[i] as View
                if (currentBean.index==currentBean.fixRow){
                    continue
                }
                val currentTranslationX = imageView.translationX
                var targetTranslationX = 0f
                if (currentBean.fixRow==0||currentBean.fixRow==moveBeans.size-1){  //当固定列在头或者尾的时候
                    targetTranslationX = if (currentBean.index!! < currentBean.fixRow!!) {
                        currentTranslationX - ((currentBean.fixRow!! - currentBean.index!!)*currentBean.moveDistance!!)
                    } else {
                        (currentBean.moveDistance!! * (currentBean.index!!)).toFloat()
                    }
                }else{ // 当固定列在中间
                    targetTranslationX = if (currentBean.index!! < currentBean.fixRow!!) {
                        currentTranslationX - ((currentBean.fixRow!! - currentBean.index!!)*currentBean.moveDistance!!)
                    } else {
                        (currentBean.moveDistance!! * (currentBean.index!!-currentBean.fixRow!!)).toFloat()
                    }
                }
                var moveAnimator = ObjectAnimator.ofFloat(
                    imageView,
                    "translationX",
                    currentTranslationX,
                    targetTranslationX
                )
                moveAnimators[i] = moveAnimator
                moveAnimator?.duration = moveTime // Set the duration of the animation in milliseconds
                moveAnimator?.addListener(object : Animator.AnimatorListener{
                    override fun onAnimationStart(animation: Animator) {
                    }

                    override fun onAnimationEnd(animation: Animator) {
                        currentBean.moveStatus = 0 // Set it to 2 for example, adjust as needed
                        if (index < currentBean.fixRow!!) {
                            // If the current index is less than fixRow, set it to 1 (move right)
                            if (currentBean.orientation==0){
                                currentBean.orientation = 1
                            }else if (currentBean.orientation==1){
                                currentBean.orientation = 0
                            }
                        } else {
                            // If the current index is greater than fixRow, set it to 0 (move left)
                            if (currentBean.orientation==1){
                                currentBean.orientation = 0
                            }else if (currentBean.orientation==0){
                                currentBean.orientation = 1
                            }
                        }
                        Log.e("输出","移动：${moveBeans.toString()}")
                    }

                    override fun onAnimationCancel(animation: Animator) {

                    }

                    override fun onAnimationRepeat(animation: Animator) {
                    }

                })
                moveAnimator?.start()
            }
            return
        }

        val rowBeam = moveBeans[index]
        val imageView: View = moveView[index] as View

        if (index == rowBeam.fixRow){
            Toast.makeText(this,"固定列不可移动",Toast.LENGTH_SHORT).show()
            return
        }

        if (rowBeam.moveStatus == 0) {
            rowBeam.moveStatus = 1
            val currentTranslationX = imageView.translationX
            val targetTranslationX = if (index < rowBeam.fixRow!!) {
                if (rowBeam.orientation == 0){
                    currentTranslationX - rowBeam.moveDistance!!.toFloat()
                }else{
                    currentTranslationX + rowBeam.moveDistance!!.toFloat()
                }
            } else {
                if (rowBeam.orientation == 1) {
                    currentTranslationX + rowBeam.moveDistance!!.toFloat()
                }else{
                    currentTranslationX - rowBeam.moveDistance!!.toFloat()
                }
            }

            var moveAnimator = ObjectAnimator.ofFloat(
                imageView,
                "translationX",
                currentTranslationX,
                targetTranslationX
            )
            moveAnimators[index] = moveAnimator
//            moveAnimator?.duration = moveTime
//            moveAnimator?.addListener(object : Animator.AnimatorListener {
//                override fun onAnimationStart(animation: Animator) {
//                    if (rowBeam.isMiddle) {
//                        Handler().postDelayed({
//                            stopAnimation(false)
//                        }, moveTime / 2)
//                    }
//                }
//
//                override fun onAnimationEnd(animation: Animator) {
//                    rowBeam.moveStatus = 0
//                    if (index < rowBeam.fixRow!!) {
//                        if (rowBeam.orientation==0){
//                            rowBeam.orientation = 1
//                        }else if (rowBeam.orientation==1){
//                            rowBeam.orientation = 0
//                        }
//                    } else {
//                        if (rowBeam.orientation==1){
//                            rowBeam.orientation = 0
//                        }else if (rowBeam.orientation==0){
//                            rowBeam.orientation = 1
//                        }
//                    }
//                    Log.e("输出111","移动：${moveBeans.toString()}")
//                }
//
//                override fun onAnimationCancel(animation: Animator) {
//                    rowBeam.moveStatus = 0 // 设置为你需要的状态
//                    moveAnimator?.currentPlayTime = 0 // 将动画时间设置为0，即恢复到初始状态
//                }
//
//                override fun onAnimationRepeat(animation: Animator) {
//                }
//            })
            var animatorSet = AnimatorSet()
            animatorSet.play(moveAnimator)
            animatorSet.duration = moveTime

            animatorSet.addListener(object :Animator.AnimatorListener{
                override fun onAnimationStart(animation: Animator) {
                    if (rowBeam.isMiddle) {
                        Handler().postDelayed({
                            stopAnimation(false)
                        }, moveTime / 2)
                    }
                }

                override fun onAnimationEnd(animation: Animator) {
                    rowBeam.moveStatus = 0
                    if (index < rowBeam.fixRow!!) {
                        if (rowBeam.orientation==0){
                            rowBeam.orientation = 1
                        }else if (rowBeam.orientation==1){
                            rowBeam.orientation = 0
                        }
                    } else {
                        if (rowBeam.orientation==1){
                            rowBeam.orientation = 0
                        }else if (rowBeam.orientation==0){
                            rowBeam.orientation = 1
                        }
                    }
                    Log.e("输出000","移动：${moveBeans.toString()}")
                }

                override fun onAnimationCancel(animation: Animator) {
                    rowBeam.moveStatus = 0 // 设置为你需要的状态
                    moveAnimator?.currentPlayTime = 0 // 将动画时间设置为0，即恢复到初始状态
                }

                override fun onAnimationRepeat(animation: Animator) {
                }

            })
//            moveAnimator?.start()
            moveAnimatorSets[index] = animatorSet

            animatorSet.start()
        } else if (rowBeam.moveStatus == 1) {
            stopAnimation(false)
        } else if (rowBeam.moveStatus == 2) {
            if (isMoveLeft){
                if (rowBeam.orientation==0){
                    resumeAnimation(index)
                }else {
                    cancleAnimation(index)
                }
            }else{
                if (rowBeam.orientation==1){
                    resumeAnimation(index)
                }else {
                    cancleAnimation(index)
                }
            }
        }
    }

    //停止移动 再次移动需点击继续
    private fun stopAnimation(isAllStop:Boolean) {
        if (isAllStop){
            for (index in moveBeans.indices){
                val rowBeam = moveBeans[index]
                rowBeam.moveStatus = 2
                var currentMoveAnimator = moveAnimators[index]
                var currentMoveAnimatorSet = moveAnimatorSets[index]
                if (currentMoveAnimatorSet?.isRunning == true) {
                    currentMoveAnimatorSet.pause()
                }
            }
        }else{
            for (index in moveBeans.indices){
                val rowBeam = moveBeans[index]
                if (rowBeam.moveStatus==1){
                    if (rowBeam.isMiddle){
                        rowBeam.moveStatus = 2
                        var currentMoveAnimator = moveAnimators[index]
                        var currentMoveAnimatorSet = moveAnimatorSets[index]
                        if (currentMoveAnimatorSet?.isRunning == true) {
                            currentMoveAnimatorSet.pause()
                        }
                    }
                }
            }
        }

    }

    /**
     * 恢复动画执行
     * @param resumeIndex -1 全部执行
     */
    private fun resumeAnimation(resumeIndex:Int) {
        if (resumeIndex==-1){
            for (index in moveView.indices) {
                val rowBeam = moveBeans[index]
                if (rowBeam.moveStatus == 2) { // Resume animation only if it was paused
                    rowBeam.moveStatus = 1 // Set moveStatus to 1 (resuming)
                    var currentMoveAnimator = moveAnimators[index]
                    var currentMoveAnimatorSet = moveAnimatorSets[index]
                    if (currentMoveAnimatorSet?.isPaused ==true){
                        currentMoveAnimatorSet?.resume()
                    }
                }
            }
        }else{
            val rowBeam = moveBeans[resumeIndex]
            if (rowBeam.moveStatus == 2) { // Resume animation only if it was paused
                rowBeam.moveStatus = 1 // Set moveStatus to 1 (resuming)
                var currentMoveAnimator = moveAnimators[resumeIndex]
                var currentMoveAnimatorSet = moveAnimatorSets[resumeIndex]
                if (currentMoveAnimatorSet?.isPaused ==true){
                    currentMoveAnimatorSet?.resume()
                    currentMoveAnimatorSet.addListener(object :Animator.AnimatorListener{
                        override fun onAnimationStart(animation: Animator) {
                        }

                        override fun onAnimationEnd(animation: Animator) {
                        }

                        override fun onAnimationCancel(animation: Animator) {
                        }

                        override fun onAnimationRepeat(animation: Animator) {
                        }

                    })
                }
            }
        }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun cancleAnimation(cancleIndex:Int){
        if (cancleIndex==-1){
            for (index in moveView.indices) {
                val rowBeam = moveBeans[index]
                if (rowBeam.moveStatus == 2) { // Resume animation only if it was paused
                    rowBeam.moveStatus = 1 // Set moveStatus to 1 (resuming)
                    var currentMoveAnimator = moveAnimators[index]
                    var currentMoveAnimatorSet = moveAnimatorSets[index]
                    if (currentMoveAnimatorSet?.isPaused ==true){
                        currentMoveAnimatorSet?.cancel()
                        currentMoveAnimatorSet?.currentPlayTime = 0
                    }
                }
            }
        }else{
            val rowBeam = moveBeans[cancleIndex]
            if (rowBeam.moveStatus == 2) {
                rowBeam.moveStatus = 1
                var currentMoveAnimator = moveAnimators[cancleIndex]
                var currentMoveAnimatorSet = moveAnimatorSets[cancleIndex]
                if (currentMoveAnimatorSet?.isPaused ==true){
                    currentMoveAnimatorSet?.cancel()
                    currentMoveAnimatorSet?.currentPlayTime = 0
                }
            }
        }
    }
    private fun resetAnimation(index:Int) {
        val rowBeam = moveBeans[index]
        val imageView: View = moveView[index] as View
        if (rowBeam.fixRow==index){
            return
        }
        val resetAnimator = ObjectAnimator.ofFloat(imageView, "translationX", 0f)
        resetAnimator.duration = moveTime

        resetAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {}
            override fun onAnimationEnd(animation: Animator) {
                rowBeam.moveStatus = 0
                if (index < rowBeam.fixRow!!) {
                    rowBeam.orientation = 0
                } else if (index > rowBeam.fixRow!!) {
                    rowBeam.orientation = 1
                }
                Log.e("输出", "reset for index $index: ${moveBeans.toString()}")
            }

            override fun onAnimationCancel(animation: Animator) {}

            override fun onAnimationRepeat(animation: Animator) {}
        })

        resetAnimator.start()
    }
    private fun resetAnimation() {
        for (index in moveView.indices) {
            val rowBeam = moveBeans[index]
            val imageView: View = moveView[index] as View
            if (rowBeam.fixRow==index){
                continue
            }
            val resetAnimator = ObjectAnimator.ofFloat(imageView, "translationX", 0f)
            resetAnimator.duration = moveTime
            resetAnimator.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animation: Animator) {}

                override fun onAnimationEnd(animation: Animator) {
                    rowBeam.moveStatus = 0
                    if (index < rowBeam.fixRow!!) {
                        rowBeam.orientation = 0
                    } else if (index > rowBeam.fixRow!!) {
                        rowBeam.orientation = 1
                    }
                    Log.e("输出", "reset for index $index: ${moveBeans.toString()}")
                }

                override fun onAnimationCancel(animation: Animator) {}

                override fun onAnimationRepeat(animation: Animator) {}
            })
            resetAnimator.start()
        }
    }

}