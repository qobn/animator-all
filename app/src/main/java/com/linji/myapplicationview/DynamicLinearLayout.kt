package com.linji.myapplicationview
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.widget.ImageView
import android.widget.LinearLayout



class DynamicLinearLayout(context: Context) : LinearLayout(context) {
    private val cabinets = mutableListOf<Cabinet>()
    private var lastClickedIndex: Int = -1
    private var animatorSet: AnimatorSet? = null

    init {
        orientation = HORIZONTAL
    }

    fun addCabinet(cabinet: Cabinet) {
        cabinets.add(cabinet)
        val imageView = ImageView(context).apply {
            // Configure ImageView properties here
            setBackgroundResource(R.mipmap.ic_launcher)
        }
        cabinet.view = imageView
        addView(imageView)
        imageView.setOnClickListener { view ->
            val index = indexOfChild(view)
            handleCabinetClick(index)
        }
    }

    fun handleCabinetClick(index: Int) {
        lastClickedIndex = index
        val clickedCabinet = cabinets[index]
        if (!clickedCabinet.fixed) {
            determineDirectionAndMove(index)
        }
        // Trigger any necessary animations here
    }

    private fun determineDirectionAndMove(index: Int) {
        val direction = if (index > lastClickedIndex) 1 else -1
        for (i in cabinets.indices) {
            if (cabinets[i].fixed) continue // Skip fixed cabinets
            cabinets[i].moving = true
            cabinets[i].direction = direction
        }
        startCabinetMovementAnimation()
    }

    private fun startCabinetMovementAnimation() {
        animatorSet?.cancel() // Cancel the previous animation if any

        val animations = mutableListOf<ObjectAnimator>()
        for (cabinet in cabinets) {
            if (cabinet.moving) {
                val translationX = cabinet.view?.translationX ?: 0f
                val targetTranslationX = translationX + cabinet.direction * CABINET_MOVE_DISTANCE
                val animator = ObjectAnimator.ofFloat(cabinet.view, "translationX", translationX, targetTranslationX)
                animations.add(animator)
            }
        }

        animatorSet = AnimatorSet().apply {
            playTogether(animations as Collection<Animator>?)
            duration = CABINET_ANIMATION_DURATION
            start()
        }

        animatorSet?.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                adjustCabinetsAfterMove()
            }
        })
    }

    private fun adjustCabinetsAfterMove() {
        // Implement your logic to adjust cabinets after the movement
        // This may involve checking for overlapping and adjusting positions
        for (cabinet in cabinets) {
            cabinet.moving = false
            cabinet.direction = 0
        }
    }

    fun setCabinetFixed(index: Int, fixed: Boolean) {
        if (index in cabinets.indices) {
            cabinets[index].fixed = fixed
        }
    }

    companion object {
        const val CABINET_MOVE_DISTANCE = 100f // Adjust this based on your layout
        const val CABINET_ANIMATION_DURATION = 500L // Animation duration in milliseconds
    }
}
