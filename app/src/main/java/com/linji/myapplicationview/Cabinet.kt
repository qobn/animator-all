package com.linji.myapplicationview

import android.widget.ImageView

class Cabinet {
    var index: Int =0
    var fixed: Boolean = false
    var moving: Boolean = false
    var direction: Int = 0
    var view: ImageView? = null
}